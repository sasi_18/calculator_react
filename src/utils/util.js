export const performCalculation = {
    '/': (firstOperand, secondOperand) => firstOperand / secondOperand,

    '*': (firstOperand, secondOperand) => firstOperand * secondOperand,

    '+': (firstOperand, secondOperand) => Number(firstOperand) + Number(secondOperand),

    '-': (firstOperand, secondOperand) => firstOperand - secondOperand,

    '=': (firstOperand, secondOperand) => secondOperand,

    '%*': (firstOperand, secondOperand) => (firstOperand * 0.01 * secondOperand),

    '%': (firstOperand, secondOperand) => (firstOperand * 0.01)
};

