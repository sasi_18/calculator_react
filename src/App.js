import React, { Component } from 'react';
import Keypad from './components/keypad';

class App extends Component {

  render() {
    return (
      <div className="main-wrapper">
        <Keypad buttonPressed={this.buttonPressed} />
      </div>
    );
  }
}

export default App;
