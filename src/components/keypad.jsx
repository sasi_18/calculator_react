import React, { Component } from 'react';
import Output from "./output";
import { performCalculation } from '../utils/util';
import "../../src/App.css";


class Keypad extends Component {
    state = {
        result: '',
        numberOne: '',
        numberTwo: '',
        operator: '',
    }

    //buttonpresssed function

    buttonPressed = event => {

        const buttonName = event.target.name;
        if (buttonName === '=') {
            this.calculate()
        } else
            if (buttonName === 'C') {
                this.reset()
            } else if (buttonName === 'oppositeSign') {
                if (!this.state.numberTwo) {
                    this.setState(prevState => ({ numberOne: ~prevState.numberOne + 1 }))
                } else {
                    this.setState(prevState => ({ numberTwo: ~prevState.numberTwo + 1 }))
                }
            }
            else if (buttonName === '+' || buttonName === '-' || buttonName === '*' || buttonName === '/' || buttonName === '%') {
                if (this.state.result) {
                    this.setState(prevState => ({
                        numberOne: prevState.result,
                        numberTwo: '',
                        operator: buttonName,
                        result: '',
                    }));
                } else {
                    this.setState(prevState => ({
                        operator: buttonName,
                        numberOne: prevState.numberOne ? prevState.numberOne : 0,
                    }))
                }
            }
            else {
                if (this.state.operator) {
                    this.setState(prevState => ({
                        numberTwo: prevState.numberTwo + buttonName,
                        operator: prevState.operator === '%' ? '%*' : prevState.operator,
                    }));
                } else {
                    this.setState(prevState => ({
                        numberOne: prevState.numberOne + buttonName
                    }))
                }
            }
    }

    //reset the calculator

    reset = () => {
        this.setState({
            result: '',
            numberOne: '',
            numberTwo: '',
            operator: '',
        })
    }

    //calculating the results

    calculate = () => {
        try {
            let result;
            if (this.state.operator) {
                result = performCalculation[this.state.operator](this.state.numberOne, this.state.numberTwo);
            } else {
                result = this.state.numberOne;
            }
            this.setState({
                result,
            })
        } catch (e) {
            this.setState({ result: "error" })
        }

    }

    render() {
        return (
            <div>
                <div className="calc-content">
                    <div className="calc-wrapper">
                        <Output {...this.state} result={this.state.result} />
                        <div className="keypad">
                            <div className="keypad-content">
                                <button className="btn" name="C" onClick={this.buttonPressed}>C</button>
                                <button className="btn" name="oppositeSign" onClick={this.buttonPressed}>+/-</button>
                                <button className="btn" name="%" onClick={this.buttonPressed}>%</button>
                                <button className="sign" name="/" onClick={this.buttonPressed}>/</button>
                            </div>
                            <div className="keypad-content">
                                <button className="btn" name="7" onClick={this.buttonPressed}>7</button>
                                <button className="btn" name="8" onClick={this.buttonPressed}>8</button>
                                <button className="btn" name="9" onClick={this.buttonPressed}>9</button>
                                <button className="sign" name="*" onClick={this.buttonPressed}>*</button>
                            </div>
                            <div className="keypad-content">
                                <button className="btn" name="4" onClick={this.buttonPressed}>4</button>
                                <button className="btn" name="5" onClick={this.buttonPressed}>5</button>
                                <button className="btn" name="6" onClick={this.buttonPressed}>6</button>
                                <button className="sign" name="-" onClick={this.buttonPressed}>-</button>
                            </div>
                            <div className="keypad-content">
                                <button className="btn" name="1" onClick={this.buttonPressed}>1</button>
                                <button className="btn" name="2" onClick={this.buttonPressed}>2</button>
                                <button className="btn" name="3" onClick={this.buttonPressed}>3</button>
                                <button className="sign" name="+" onClick={this.buttonPressed}>+</button>
                            </div>
                            <div className="keypad-content">
                                <button className="btn" name="0" onClick={this.buttonPressed}>0</button>
                                <button className="btn" name="." onClick={this.buttonPressed}>.</button>
                                <button className="equal" name="=" onClick={this.buttonPressed}>=</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

export default Keypad

