import React, { Component } from 'react';

class Output extends Component {

    render() {
        return (
            <div className="output-contain">
                <div className="number-display">{this.props.numberOne} {this.props.operator} {this.props.numberTwo}</div>
                <div className="output-display">{this.props.result}</div>
            </div>
        )
    }
}

export default Output